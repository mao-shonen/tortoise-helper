import os
from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional, Type
from jinja2 import Environment, FileSystemLoader
from tortoise import fields as base_fields
from tortoise.fields.base import Field as BaseField
from tortoise_helper import fields as extend_fields


env = Environment(
    loader=FileSystemLoader('./templates'),
    trim_blocks=True,
    lstrip_blocks=True,
)

template = env.get_template('fields.pt.j2')


def get_annotation_name(annotation: Any) -> str:
    if isinstance(annotation, type):
        return annotation.__name__
    else:
        return repr(annotation)


@dataclass()
class FieldDeclare:
    type: str
    expand: bool = False
    extend_params: List[str] = field(default_factory=lambda: ['*'])
    doc: Optional[str] = None


fields: Dict[str, FieldDeclare] = dict(
    BooleanField=FieldDeclare(
        type='bool',
    ),
    # enum
    CharEnumField=FieldDeclare(
        type='EnumType',
        extend_params=[
            'enum_type: Type[EnumType]',
            'max_length: int = 0',
            '*',
        ],
    ),
    IntEnumField=FieldDeclare(
        type='IntEnumType',
        extend_params=[
            'enum_type: Type[IntEnumType]',
            '*',
        ],
    ),
    # number
    SmallIntField=FieldDeclare(
        type='int',
    ),
    IntField=FieldDeclare(
        type='int',
    ),
    BigIntField=FieldDeclare(
        type='int',
    ),
    FloatField=FieldDeclare(
        type='float',
    ),
    DecimalField=FieldDeclare(
        type='Decimal',
        extend_params=[
            'max_digits: int',
            'decimal_places: int',
            '*',
        ],
    ),
    # string & bytes
    BinaryField=FieldDeclare(
        type='bytes',
    ),
    CharField=FieldDeclare(
        type='str',
        extend_params=[
            'max_length: int',
            '*',
        ],
    ),
    TextField=FieldDeclare(
        type='str',
    ),
    JSONField=FieldDeclare(
        type='Any',
        extend_params=[
            '*',
            'encoder: Any = None',
            'decoder: Any = None',
        ],
    ),
    UUIDField=FieldDeclare(
        type='UUID',
    ),
    # datetime
    DateField=FieldDeclare(
        type='date',
    ),
    DatetimeField=FieldDeclare(
        type='datetime',
        extend_params=[
            '*',
            'auto_now: bool = False',
            'auto_now_add: bool = False',
        ],
    ),
    TimeDeltaField=FieldDeclare(
        type='timedelta',
    ),
    # relational
    ForeignKeyField=FieldDeclare(
        type='ForeignKeyRelation[Any]',
        extend_params=[
            'model_name: str',
            '*',
            'related_name: Optional[Union[str, Literal[False]]] = None',
            'on_delete: RelationDeletePolicy = CASCADE',
            'to_field: Optional[str] = None',
            'db_constraint: bool = True',
        ],
    ),
    OneToOneField=FieldDeclare(
        type='OneToOneRelation[Any]',
        extend_params=[
            'model_name: str',
            '*',
            'related_name: Optional[Union[str, Literal[False]]] = None',
            'on_delete: RelationDeletePolicy = CASCADE',
            'to_field: Optional[str] = None',
            'db_constraint: bool = True',
        ],
    ),
    ManyToManyField=FieldDeclare(
        type='ManyToManyRelation[Any]',
        extend_params=[
            'model_name: str',
            '*',
            'related_name: Optional[Union[str, Literal[False]]] = None',
            'on_delete: RelationDeletePolicy = CASCADE',
            'to_field: Optional[str] = None',
            'db_constraint: bool = True',
            'through: Optional[str] = None',
            'forward_key: Optional[str] = None',
            'backward_key: str = ""',
        ],
    ),
    # extend
    UnsignedIntField=FieldDeclare(
        expand=True,
        type='int',
    ),
    UnsignedBigIntField=FieldDeclare(
        expand=True,
        type='int',
    ),
    UnsignedSmallIntField=FieldDeclare(
        expand=True,
        type='int',
    ),
)


lost_fields: List[str] = []


for field_name, field in fields.items():
    ref_fields = extend_fields if field.expand else base_fields

    base_field: Type[BaseField[Any]] = getattr(ref_fields, field_name)
    # copy doc
    if base_field.__doc__:
        field.doc = base_field.__doc__.strip()


with open(os.path.join('tortoise_helper', 'fields', 'typed_fields.py'), 'w') as f:
    data = template.render(
        __all__=list(base_fields.__all__),
        fields=fields,
    )
    f.write('"""\ngenerated from jinja2 template\n"""\n\n')
    f.write(data)
