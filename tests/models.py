import os
import binascii
from tortoise_helper import Model, fields, ModelMeta


def generate_token() -> str:
    return binascii.hexlify(os.urandom(16)).decode("ascii")


class Tournament(Model):
    id = fields.SmallIntField(pk=True)
    name = fields.CharField(max_length=255)
    desc = fields.TextField(null=True)
    created = fields.DatetimeField(auto_now_add=True, index=True)

    events: fields.ReverseRelation["Event"]
    minrelations: fields.ReverseRelation["MinRelation"]
    uniquetogetherfieldswithfks: fields.ReverseRelation["UniqueTogetherFieldsWithFK"]

    class PydanticMeta:
        exclude = ("minrelations", "uniquetogetherfieldswithfks")

    def __str__(self):
        return self.name


class Reporter(Model):
    """Whom is assigned as the reporter"""

    id = fields.IntField(pk=True)
    name = fields.TextField()

    events: fields.ReverseRelation["Event"]

    Meta = ModelMeta('re_port_er')

    def __str__(self):
        return self.name


class Event(Model):
    """Events on the calendar"""

    id = fields.BigIntField(pk=True)
    # event_id = fields.BigIntField()
    #: The name
    name = fields.TextField()
    #: What tournaments is a happenin'
    tournament: fields.ForeignKeyRelation["Tournament"] = fields.ForeignKeyField(
        "models.Tournament", related_name="events"
    )
    reporter: fields.ForeignKeyNullableRelation[Reporter] = fields.ForeignKeyField(
        "models.Reporter", null=True
    )
    participants: fields.ManyToManyRelation["Team"] = fields.ManyToManyField(
        "models.Team",
        related_name="events",
        through="event_team",
        backward_key="idEvent",
    )
    modified = fields.DatetimeField(auto_now=True)
    token = fields.TextField(default=generate_token)
    alias = fields.IntField(null=True)

    Meta = ModelMeta(ordering=["name"])

    def __str__(self):
        return self.name


class Team(Model):
    """
    Team that is a playing
    """

    id = fields.IntField(pk=True)
    name = fields.TextField()

    events: fields.ManyToManyRelation[Event]
    minrelation_through: fields.ManyToManyRelation["MinRelation"]
    alias = fields.IntField(null=True)

    Meta = ModelMeta(ordering=["id"])

    class PydanticMeta:
        exclude = ("minrelations",)

    def __str__(self):
        return self.name


class MinRelation(Model):
    id = fields.IntField(pk=True)
    tournament: fields.ForeignKeyRelation[Tournament] = fields.ForeignKeyField(
        "models.Tournament"
    )
    participants: fields.ManyToManyRelation[Team] = fields.ManyToManyField(
        "models.Team"
    )
