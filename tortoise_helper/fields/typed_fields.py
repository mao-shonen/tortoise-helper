"""
generated from jinja2 template
"""

from typing import Any, Optional, List, Type, TypeVar, Callable, Union, overload
from typing_extensions import Literal
from enum import Enum, IntEnum
from decimal import Decimal
from datetime import datetime, date, timedelta
from uuid import UUID
from tortoise.validators import Validator
from tortoise.fields.base import CASCADE
from tortoise.fields.relational import (
    ForeignKeyRelation,
    ManyToManyRelation,
    OneToOneRelation,
)
from .extends import RelationDeletePolicy


__all__ = [
    'BooleanField',
    'CharEnumField',
    'IntEnumField',
    'SmallIntField',
    'IntField',
    'BigIntField',
    'FloatField',
    'DecimalField',
    'BinaryField',
    'CharField',
    'TextField',
    'JSONField',
    'UUIDField',
    'DateField',
    'DatetimeField',
    'TimeDeltaField',
    'ForeignKeyField',
    'OneToOneField',
    'ManyToManyField',
    'UnsignedIntField',
    'UnsignedBigIntField',
    'UnsignedSmallIntField',
]

EnumType = TypeVar('EnumType', bound=Enum)
IntEnumType = TypeVar('IntEnumType', bound=IntEnum)



@overload
def BooleanField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[bool, Callable[..., bool]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->bool:
    ...
@overload
def BooleanField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[bool, Callable[..., bool]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[bool]:
    ...
def BooleanField(
    *,
    null: bool = False,
    default: Optional[Union[bool, Callable[..., bool]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[bool]:
    """
    Boolean field.
    """


@overload
def CharEnumField(
    enum_type: Type[EnumType],
    max_length: int = 0,
    *,
    null: Literal[False] = ...,
    default: Optional[Union[EnumType, Callable[..., EnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->EnumType:
    ...
@overload
def CharEnumField(
    enum_type: Type[EnumType],
    max_length: int = 0,
    *,
    null: Literal[True] = ...,
    default: Optional[Union[EnumType, Callable[..., EnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[EnumType]:
    ...
def CharEnumField(
    enum_type: Type[EnumType],
    max_length: int = 0,
    *,
    null: bool = False,
    default: Optional[Union[EnumType, Callable[..., EnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[EnumType]:
    """
    Char Enum Field

    A field representing a character enumeration.

    **Warning**: If ``max_length`` is not specified or equals to zero, the size of represented
    char fields is automatically detected. So if later you update the enum, you need to update your
    table schema as well.

    **Note**: Valid str value of ``enum_type`` is acceptable.

    ``enum_type``:
        The enum class
    ``description``:
        The description of the field. It is set automatically if not specified to a multiline list
        of "name: value" pairs.
    ``max_length``:
        The length of the created CharField. If it is zero it is automatically detected from
        enum_type.
    """


@overload
def IntEnumField(
    enum_type: Type[IntEnumType],
    *,
    null: Literal[False] = ...,
    default: Optional[Union[IntEnumType, Callable[..., IntEnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->IntEnumType:
    ...
@overload
def IntEnumField(
    enum_type: Type[IntEnumType],
    *,
    null: Literal[True] = ...,
    default: Optional[Union[IntEnumType, Callable[..., IntEnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[IntEnumType]:
    ...
def IntEnumField(
    enum_type: Type[IntEnumType],
    *,
    null: bool = False,
    default: Optional[Union[IntEnumType, Callable[..., IntEnumType]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[IntEnumType]:
    """
    Enum Field

    A field representing an integer enumeration.

    The description of the field is set automatically if not specified to a multiline list of
    "name: value" pairs.

    **Note**: Valid int value of ``enum_type`` is acceptable.

    ``enum_type``:
        The enum class
    ``description``:
        The description of the field. It is set automatically if not specified to a multiline list
        of "name: value" pairs.
    """


@overload
def SmallIntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def SmallIntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def SmallIntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Small integer field. (16-bit signed)

    ``pk`` (bool):
        True if field is Primary Key.
    """


@overload
def IntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def IntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def IntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Integer field. (32-bit signed)

    ``pk`` (bool):
        True if field is Primary Key.
    """


@overload
def BigIntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def BigIntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def BigIntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Big integer field. (64-bit signed)

    ``pk`` (bool):
        True if field is Primary Key.
    """


@overload
def FloatField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[float, Callable[..., float]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->float:
    ...
@overload
def FloatField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[float, Callable[..., float]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[float]:
    ...
def FloatField(
    *,
    null: bool = False,
    default: Optional[Union[float, Callable[..., float]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[float]:
    """
    Float (double) field.
    """


@overload
def DecimalField(
    max_digits: int,
    decimal_places: int,
    *,
    null: Literal[False] = ...,
    default: Optional[Union[Decimal, Callable[..., Decimal]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Decimal:
    ...
@overload
def DecimalField(
    max_digits: int,
    decimal_places: int,
    *,
    null: Literal[True] = ...,
    default: Optional[Union[Decimal, Callable[..., Decimal]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[Decimal]:
    ...
def DecimalField(
    max_digits: int,
    decimal_places: int,
    *,
    null: bool = False,
    default: Optional[Union[Decimal, Callable[..., Decimal]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[Decimal]:
    """
    Accurate decimal field.

    You must provide the following:

    ``max_digits`` (int):
        Max digits of significance of the decimal field.
    ``decimal_places`` (int):
        How many of those significant digits is after the decimal point.
    """


@overload
def BinaryField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[bytes, Callable[..., bytes]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->bytes:
    ...
@overload
def BinaryField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[bytes, Callable[..., bytes]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[bytes]:
    ...
def BinaryField(
    *,
    null: bool = False,
    default: Optional[Union[bytes, Callable[..., bytes]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[bytes]:
    """
    Binary field.

    This is for storing ``bytes`` objects.
    Note that filter or queryset-update operations are not supported.
    """


@overload
def CharField(
    max_length: int,
    *,
    null: Literal[False] = ...,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->str:
    ...
@overload
def CharField(
    max_length: int,
    *,
    null: Literal[True] = ...,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[str]:
    ...
def CharField(
    max_length: int,
    *,
    null: bool = False,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[str]:
    """
    Character field.

    You must provide the following:

    ``max_length`` (int):
        Maximum length of the field in characters.
    """


@overload
def TextField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->str:
    ...
@overload
def TextField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[str]:
    ...
def TextField(
    *,
    null: bool = False,
    default: Optional[Union[str, Callable[..., str]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[str]:
    """
    Large Text field.
    """


@overload
def JSONField(
    *,
    encoder: Any = None,
    decoder: Any = None,
    null: Literal[False] = ...,
    default: Optional[Union[Any, Callable[..., Any]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Any:
    ...
@overload
def JSONField(
    *,
    encoder: Any = None,
    decoder: Any = None,
    null: Literal[True] = ...,
    default: Optional[Union[Any, Callable[..., Any]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[Any]:
    ...
def JSONField(
    *,
    encoder: Any = None,
    decoder: Any = None,
    null: bool = False,
    default: Optional[Union[Any, Callable[..., Any]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[Any]:
    """
    JSON field.

    This field can store dictionaries or lists of any JSON-compliant structure.

    You can specify your own custom JSON encoder/decoder, leaving at the default should work well.
    If you have ``python-rapidjson`` installed, we default to using that,
    else the default ``json`` module will be used.

    ``encoder``:
        The custom JSON encoder.
    ``decoder``:
        The custom JSON decoder.
    """


@overload
def UUIDField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[UUID, Callable[..., UUID]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->UUID:
    ...
@overload
def UUIDField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[UUID, Callable[..., UUID]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[UUID]:
    ...
def UUIDField(
    *,
    null: bool = False,
    default: Optional[Union[UUID, Callable[..., UUID]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[UUID]:
    """
    UUID Field

    This field can store uuid value.

    If used as a primary key, it will auto-generate a UUID4 by default.
    """


@overload
def DateField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[date, Callable[..., date]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->date:
    ...
@overload
def DateField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[date, Callable[..., date]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[date]:
    ...
def DateField(
    *,
    null: bool = False,
    default: Optional[Union[date, Callable[..., date]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[date]:
    """
    Date field.
    """


@overload
def DatetimeField(
    *,
    auto_now: bool = False,
    auto_now_add: bool = False,
    null: Literal[False] = ...,
    default: Optional[Union[datetime, Callable[..., datetime]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->datetime:
    ...
@overload
def DatetimeField(
    *,
    auto_now: bool = False,
    auto_now_add: bool = False,
    null: Literal[True] = ...,
    default: Optional[Union[datetime, Callable[..., datetime]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[datetime]:
    ...
def DatetimeField(
    *,
    auto_now: bool = False,
    auto_now_add: bool = False,
    null: bool = False,
    default: Optional[Union[datetime, Callable[..., datetime]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[datetime]:
    """
    Datetime field.

    ``auto_now`` and ``auto_now_add`` is exclusive.
    You can opt to set neither or only ONE of them.

    ``auto_now`` (bool):
        Always set to ``datetime.utcnow()`` on save.
    ``auto_now_add`` (bool):
        Set to ``datetime.utcnow()`` on first save only.
    """


@overload
def TimeDeltaField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[timedelta, Callable[..., timedelta]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->timedelta:
    ...
@overload
def TimeDeltaField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[timedelta, Callable[..., timedelta]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[timedelta]:
    ...
def TimeDeltaField(
    *,
    null: bool = False,
    default: Optional[Union[timedelta, Callable[..., timedelta]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[timedelta]:
    """
    A field for storing time differences.
    """


@overload
def ForeignKeyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: Literal[False] = ...,
    default: Optional[Union[ForeignKeyRelation[Any], Callable[..., ForeignKeyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->ForeignKeyRelation[Any]:
    ...
@overload
def ForeignKeyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: Literal[True] = ...,
    default: Optional[Union[ForeignKeyRelation[Any], Callable[..., ForeignKeyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[ForeignKeyRelation[Any]]:
    ...
def ForeignKeyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: bool = False,
    default: Optional[Union[ForeignKeyRelation[Any], Callable[..., ForeignKeyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[ForeignKeyRelation[Any]]:
    """
    ForeignKey relation field.

    This field represents a foreign key relation to another model.

    See :ref:`foreign_key` for usage information.

    You must provide the following:

    ``model_name``:
        The name of the related model in a :samp:`'{app}.{model}'` format.

    The following is optional:

    ``related_name``:
        The attribute name on the related model to reverse resolve the foreign key.
    ``on_delete``:
        One of:
            ``field.CASCADE``:
                Indicate that the model should be cascade deleted if related model gets deleted.
            ``field.RESTRICT``:
                Indicate that the related model delete will be restricted as long as a
                foreign key points to it.
            ``field.SET_NULL``:
                Resets the field to NULL in case the related model gets deleted.
                Can only be set if field has ``null=True`` set.
            ``field.SET_DEFAULT``:
                Resets the field to ``default`` value in case the related model gets deleted.
                Can only be set is field has a ``default`` set.
    ``to_field``:
        The attribute name on the related model to establish foreign key relationship.
        If not set, pk is used
    ``db_constraint``:
        Controls whether or not a constraint should be created in the database for this foreign key.
        The default is True, and that’s almost certainly what you want; setting this to False can be very bad for data integrity.
    """


@overload
def OneToOneField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: Literal[False] = ...,
    default: Optional[Union[OneToOneRelation[Any], Callable[..., OneToOneRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->OneToOneRelation[Any]:
    ...
@overload
def OneToOneField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: Literal[True] = ...,
    default: Optional[Union[OneToOneRelation[Any], Callable[..., OneToOneRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[OneToOneRelation[Any]]:
    ...
def OneToOneField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    null: bool = False,
    default: Optional[Union[OneToOneRelation[Any], Callable[..., OneToOneRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[OneToOneRelation[Any]]:
    """
    OneToOne relation field.

    This field represents a foreign key relation to another model.

    See :ref:`one_to_one` for usage information.

    You must provide the following:

    ``model_name``:
        The name of the related model in a :samp:`'{app}.{model}'` format.

    The following is optional:

    ``related_name``:
        The attribute name on the related model to reverse resolve the foreign key.
    ``on_delete``:
        One of:
            ``field.CASCADE``:
                Indicate that the model should be cascade deleted if related model gets deleted.
            ``field.RESTRICT``:
                Indicate that the related model delete will be restricted as long as a
                foreign key points to it.
            ``field.SET_NULL``:
                Resets the field to NULL in case the related model gets deleted.
                Can only be set if field has ``null=True`` set.
            ``field.SET_DEFAULT``:
                Resets the field to ``default`` value in case the related model gets deleted.
                Can only be set is field has a ``default`` set.
    ``to_field``:
        The attribute name on the related model to establish foreign key relationship.
        If not set, pk is used
    ``db_constraint``:
        Controls whether or not a constraint should be created in the database for this foreign key.
        The default is True, and that’s almost certainly what you want; setting this to False can be very bad for data integrity.
    """


@overload
def ManyToManyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    through: Optional[str] = None,
    forward_key: Optional[str] = None,
    backward_key: str = "",
    null: Literal[False] = ...,
    default: Optional[Union[ManyToManyRelation[Any], Callable[..., ManyToManyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->ManyToManyRelation[Any]:
    ...
@overload
def ManyToManyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    through: Optional[str] = None,
    forward_key: Optional[str] = None,
    backward_key: str = "",
    null: Literal[True] = ...,
    default: Optional[Union[ManyToManyRelation[Any], Callable[..., ManyToManyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[ManyToManyRelation[Any]]:
    ...
def ManyToManyField(
    model_name: str,
    *,
    related_name: Optional[Union[str, Literal[False]]] = None,
    on_delete: RelationDeletePolicy = CASCADE,
    to_field: Optional[str] = None,
    db_constraint: bool = True,
    through: Optional[str] = None,
    forward_key: Optional[str] = None,
    backward_key: str = "",
    null: bool = False,
    default: Optional[Union[ManyToManyRelation[Any], Callable[..., ManyToManyRelation[Any]]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[ManyToManyRelation[Any]]:
    """
    ManyToMany relation field.

    This field represents a many-to-many between this model and another model.

    See :ref:`many_to_many` for usage information.

    You must provide the following:

    ``model_name``:
        The name of the related model in a :samp:`'{app}.{model}'` format.

    The following is optional:

    ``through``:
        The DB table that represents the through table.
        The default is normally safe.
    ``forward_key``:
        The forward lookup key on the through table.
        The default is normally safe.
    ``backward_key``:
        The backward lookup key on the through table.
        The default is normally safe.
    ``related_name``:
        The attribute name on the related model to reverse resolve the many to many.
    ``db_constraint``:
        Controls whether or not a constraint should be created in the database for this foreign key.
        The default is True, and that’s almost certainly what you want; setting this to False can be very bad for data integrity.
    ``on_delete``:
        One of:
            ``field.CASCADE``:
                Indicate that the model should be cascade deleted if related model gets deleted.
            ``field.RESTRICT``:
                Indicate that the related model delete will be restricted as long as a
                foreign key points to it.
            ``field.SET_NULL``:
                Resets the field to NULL in case the related model gets deleted.
                Can only be set if field has ``null=True`` set.
            ``field.SET_DEFAULT``:
                Resets the field to ``default`` value in case the related model gets deleted.
                Can only be set is field has a ``default`` set.
    """


@overload
def UnsignedIntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def UnsignedIntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def UnsignedIntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Integer field. (32-bit unsigned)
    """


@overload
def UnsignedBigIntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def UnsignedBigIntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def UnsignedBigIntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Big integer field. (64-bit unsigned)
    """


@overload
def UnsignedSmallIntField(
    *,
    null: Literal[False] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->int:
    ...
@overload
def UnsignedSmallIntField(
    *,
    null: Literal[True] = ...,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    ...
def UnsignedSmallIntField(
    *,
    null: bool = False,
    default: Optional[Union[int, Callable[..., int]]] = None,
    source_field: Optional[str] = None,
    generated: bool = False,
    pk: bool = False,
    unique: bool = False,
    index: bool = False,
    description: Optional[str] = None,
    validators: Optional[List[Validator]] = None,
    **kwargs: Any,
) ->Optional[int]:
    """
    Small integer field. (16-bit unsigned)
    """

