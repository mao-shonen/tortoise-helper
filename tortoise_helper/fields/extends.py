from typing import Any, Dict, Optional
from typing_extensions import Literal
from tortoise import fields


RelationDeletePolicy = Literal['CASCADE', 'RESTRICT', 'SET_NULL', 'SET_DEFAULT']


class UnsignedIntField(fields.IntField):
    '''
    Integer field. (32-bit unsigned)
    '''

    SQL_TYPE = 'INT UNSIGNED'

    @property
    def constraints(self) -> Dict[str, Any]:
        self.reference: Optional[fields.Field[Any]]

        return {
            'ge': 1 if self.generated or self.reference else 0,
            'le': 4294967295,
        }

    class _db_mysql(fields.IntField._db_mysql):
        GENERATED_SQL = 'INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT'


class UnsignedBigIntField(fields.BigIntField):
    '''
    Big integer field. (64-bit unsigned)
    '''

    SQL_TYPE = 'BIGINT UNSIGNED'

    @property
    def constraints(self) -> Dict[str, Any]:
        self.reference: Optional[fields.Field[Any]]

        return {
            'ge': 1 if self.generated or self.reference else 0,
            'le': 18446744073709551615,
        }

    class _db_mysql(fields.BigIntField._db_mysql):
        GENERATED_SQL = 'BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT'


class UnsignedSmallIntField(fields.SmallIntField):
    '''
    Small integer field. (16-bit unsigned)
    '''

    SQL_TYPE = 'SMALLINT UNSIGNED'

    @property
    def constraints(self) -> Dict[str, Any]:
        self.reference: Optional[fields.Field[Any]]

        return {
            'ge': 1 if self.generated or self.reference else 0,
            'le': 4294967295,
        }

    class _db_mysql(fields.SmallIntField._db_mysql):
        GENERATED_SQL = 'SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT'
