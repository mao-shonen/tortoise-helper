from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    Union,
    Optional,
    Tuple,
    Type,
    TypeVar,
    cast,
    Sequence,
)
from typing_extensions import dataclass_transform, Self
from tortoise.models import Model as TortoiseModel
from tortoise.backends.base.client import BaseDBAsyncClient
from tortoise.manager import Manager as TortoiseManager
from .types import Default


ModelT = TypeVar('ModelT')


@dataclass_transform(
    kw_only_default=True,
)
class Model(TortoiseModel):
    # todo: select_for_update

    if TYPE_CHECKING:

        @classmethod
        async def get_or_create(
            cls: Type[ModelT],
            defaults: Optional[Dict[str, Any]] = None,
            using_db: Optional[BaseDBAsyncClient] = None,
            **kwargs: Any,
        ) -> Tuple[ModelT, bool]:
            ...

        @classmethod
        async def update_or_create(
            cls: Type[ModelT],
            defaults: Optional[Dict[str, Any]] = None,
            using_db: Optional[BaseDBAsyncClient] = None,
            **kwargs: Any,
        ) -> Tuple[ModelT, bool]:
            ...

        def update_from_dict(self: Self, data: Dict[str, Any]) -> Self:
            ...


def ModelMeta(
    table: str = Default,
    table_description: str = Default,
    *,
    abstract: bool = Default,
    schema: str = Default,
    unique_together: Sequence[Union[str, Sequence[str]]] = Default,
    indexes: Sequence[Union[str, Sequence[str]]] = Default,
    ordering: Sequence[str] = Default,
    manager: Type[TortoiseManager] = Default,
    **kwargs: Any,
) -> Type[TortoiseModel.Meta]:
    '''
    ### params
    - `table`: Set this to configure a manual table name, instead of a generated one
    - `abstract`: Set to True to indicate this is an abstract class
    - `schema`: Set this to configure a schema name, where table exists
    - `table_description`: Set this to generate a comment message for the table being created for the current model
    - `unique_together`: Specify unique_together to set up compound unique indexes for sets of columns.
    - `indexes`: Specify indexes to set up compound non-unique indexes for sets of columns.
    - `ordering`: Specify ordering to set up default ordering for given model. It should be iterable of strings formatted in same way as .order_by(...) receives. If query is built with GROUP_BY clause using .annotate(...) default ordering is not applied.
    - `manager`: Specify manager to override the default manager. It should be instance of tortoise.manager.Manager or subclass.

    ### example
    ```python
    class User(Model):
        id: int = fields.IntField(pk=True)
        username: str = fields.CharField(max_length=60)
        password: str = fields.CharField(max_length=255)

        Meta = ModelMeta(
            table='users',
            table_description='users table',
        )
    ```
    '''
    options = dict(
        table=table,
        table_description=table_description,
        abstract=abstract,
        schema=schema,
        unique_together=unique_together,
        indexes=indexes,
        ordering=ordering,
        manager=manager,
        **kwargs,
    )

    options = {k: v for k, v in options.items() if not isinstance(v, type(Default))}

    meta = type(
        'Meta',
        (),
        options,
    )

    return cast(Type[TortoiseModel.Meta], meta)


def ModelPydanticMeta(
    *,
    allow_cycles: bool = Default,
    backward_relations: bool = Default,
    computed: Tuple[str, ...] = Default,
    config_class: Optional[type] = Default,
    exclude: Tuple[str, ...] = Default,
    exclude_raw_fields: bool = Default,
    include: Tuple[str, ...] = Default,
    max_recursion: int = Default,
    sort_alphabetically: bool = Default,
    **kwargs: Any,
) -> type:
    '''
    ### params
    - `allow_cycles`: Allow cycles in recursion - This can result in HUGE data - Be careful! Please use this with exclude/include and sane max_recursion
    - `backward_relations`: Use backward relations without annotations - not recommended, it can be huge data without control
    - `computed`: Computed fields can be listed here to use in pydantic model
    - `config_class`: Allows user to specify custom config class for generated model
    - `exclude`: Fields listed in this property will be excluded from pydantic model
    - `exclude_raw_fields`: If we should exclude raw fields (the ones have _id suffixes) of relations
    - `include`: If not empty, only fields this property contains will be in the pydantic model
    - `max_recursion`: Maximum recursion level allowed
    - `sort_alphabetically`: Sort fields alphabetically. If not set (or False) then leave fields in declaration order

    ### example
    ```python
    class User(Model):
        id: int = fields.IntField(pk=True)
        username: str = fields.CharField(max_length=60)
        password: str = fields.CharField(max_length=255)

        Meta = ModelMeta(
            table='users',
            table_description='users table',
        )
        PydanticMeta = ModelPydanticMeta(
            exclude=['password'],
        )
    ```
    '''

    options = dict(
        allow_cycles=allow_cycles,
        backward_relations=backward_relations,
        computed=computed,
        config_class=config_class,
        exclude=exclude,
        exclude_raw_fields=exclude_raw_fields,
        include=include,
        max_recursion=max_recursion,
        sort_alphabetically=sort_alphabetically,
        **kwargs,
    )

    options = {k: v for k, v in options.items() if not isinstance(v, type(Default))}

    PydanticMeta = type(
        'PydanticMeta',
        (),
        options,
    )

    return PydanticMeta
